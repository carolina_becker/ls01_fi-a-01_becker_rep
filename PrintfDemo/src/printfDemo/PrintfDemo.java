package printfDemo;


/*
 * Formattieren mit der printf-Anweisung
 *
 * Alternativ kann man auch mit der statischen Methode format aus
 * der Klasse String formattieren (die Formattierungszeichen sind wie
 * bei printf)
 * Walter Waldner, 2005
 */
 

public class PrintfDemo
{
  public static void main( String[] args )
  {
    int i = 123;
    // rechtsbündig (Stellen nach Bedarf)
    System.out.printf( "|%d|   |%d|\n" ,     i, -i);    // |123|   |-123|
    // rechtsbündig (mindestens 5 Stellen)
    System.out.printf( "|%5d| |%5d|\n" ,     i, -i);    // |  123| | 123|
    // linksbündig (mindestens 5 Stellen)
    System.out.printf( "|%-5d| |%-5d|\n" ,   i, -i);    // |123  | |-123 |
    // zusätzlich mit Vorzeichen (auch positive Zahlen)
    System.out.printf( "|%+-5d| |%+-5d|\n" , i, -i);    // |+123 | |-123 |
    // Auffüllen nach vorne mit Nullen
    System.out.printf( "|%05d| |%05d|\n\n",  i, -i);    // |00123| |-0123|
    // hexadezimale Darstellung in Groß- bzw. Kleinbuchstaben
    System.out.printf( "|%X| |%x|\n", 0xabc, 0xabc );   // |ABC| |abc|
    // hexadezimale Darstellung mit Null-Auffüllung und x in der Anzeige
    System.out.printf( "|%08x| |%#x|\n\n", 0xabc, 0xabc ); // |00000abc| |0xabc|


    double d = 1234.5678;
    // Standard-Ausgabe von Dezimalzahlen mit 6 Nachkommastellen
    System.out.printf( "|%f| |%f|\n" ,         d, -d);  // |1234,567800| |-1234,567800|
    // 2 Nachkommastellen
    System.out.printf( "|%.2f| |%.2f|\n" ,     d, -d);  // |1234,57| |-1234,57|
    // 10 Stellen insgesamt
    System.out.printf( "|%10f| |%10f|\n" ,     d, -d);  // |1234,567800| |-1234,567800|
    // 10 Stellen gesamt, davon 2 Nachkommastellen
    System.out.printf( "|%10.2f| |%10.2f|\n" , d, -d);  // |   1234,57| |  1234,57|
    // zusätzlich Auffüllen mit Nullen
    System.out.printf( "|%010.2f| |%010.2f|\n",d, -d);  // |0001234,57| |-001234,57|


    String s = "Java-Programm";
    // Standardausgabe
    System.out.printf( "\n|%s|\n", s );                 // |Java-Programm|
    // rechtsbündig mit 20 Stellen
    System.out.printf( "|%20s|\n", s );                 // |       Java-Programm|
    // linksbündig mit 20 Stellen
    System.out.printf( "|%-20s|\n", s );                // |Java-Programm       |
    // mindestens 7 Stellen
    System.out.printf( "|%7s|\n", s );                  // |Java-Programm|
    // höchstens 4 Stellen
    System.out.printf( "|%.4s|\n", s );                 // |Java|
    // 20 Positionen, rechtsbündig, höchstens 4 Stellen von String
    System.out.printf( "|%20.4s|\n", s );               // |                Java|
  }
}