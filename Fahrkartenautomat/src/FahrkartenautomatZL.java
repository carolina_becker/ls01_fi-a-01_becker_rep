import java.util.Scanner;

class FahrkartenautomatZL {

	/**
	 * Erfasst die Bestellung
	 * 
	 * @return Gesamter Ticketpreis
	 */
	public static double fahrkartenbestellungErfassen() {
		int anzahlTickets;
		double ticketPreis = 0.0;
		Scanner tastatur = new Scanner(System.in);

		int auswahl;

		System.out.println("Fahrkartenbestellvorgang:");
		System.out.println("===========================\n");
		System.out.println("Waehlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:");

		System.out.println("   Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
		System.out.println("   Tageskarte Regeltarif AB [8,60 EUR] (2)");
		System.out.println("   Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
		
		do {
			System.out.print("Ihre Wahl:");
			auswahl = tastatur.nextInt();
		
			switch (auswahl) {
			case 1:
				ticketPreis = 2.9;
				break;
			case 2:
				ticketPreis = 8.6;
				break;
			case 3:
				ticketPreis = 23.5;
				break;
			default:
				System.out.println(">>falsche Eingabe<<");
			}
		} while (auswahl < 1 || auswahl > 3);

		System.out.print("Anzahl der Tickets: ");
		anzahlTickets = tastatur.nextInt();

		if (ticketPreis <= 0) {
			ticketPreis = 1;
		}

		if (anzahlTickets < 1 || anzahlTickets > 10) {
			anzahlTickets = 1;
		}

		return ticketPreis * anzahlTickets;
	}

	/**
	 * Erh�lt den zu zahlenden Betrag und fragt den User ab, bis er den Betrag
	 * bezahlt hat
	 * 
	 * @param zuZahlenderBetrag, Betrag der zuzahlen ist
	 * @return R�ckgeld, wie viel �berbezahlt wurde
	 */
	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		double eingeworfenemuenze;
		double eingezahlterGesamtbetrag = 0.0;
		Scanner tastatur = new Scanner(System.in);

		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.format("Noch zu zahlen: %4.2f Euro %n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			eingeworfenemuenze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfenemuenze;
		}
		return eingezahlterGesamtbetrag - zuZahlenderBetrag;
	}

	/**
	 * Gibt animiert die Meldung aus, das bezahlt wurde
	 */
	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	/**
	 * Erhält dem Rückgabebetrag und zahlt diesen in passenden Münzen aus
	 */
	public static void rueckgeldAusgeben(double rueckgabebetrag) {

		if (rueckgabebetrag > 0.0) {
			System.out.format("Der R�ckgabebetrag in H�he von %4.2f Euro %n", rueckgabebetrag);
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (rueckgabebetrag >= 2.0) {// 2 EURO-muenzen
				System.out.println("2 EURO");
				rueckgabebetrag -= 2.0;
			}
			while (rueckgabebetrag >= 1.0) {// 1 EURO-muenzen
				System.out.println("1 EURO");
				rueckgabebetrag -= 1.0;
			}
			while (rueckgabebetrag >= 0.5) // 50 CENT-muenzen
			{
				System.out.println("50 CENT");
				rueckgabebetrag -= 0.5;
			}
			while (rueckgabebetrag >= 0.2) // 20 CENT-muenzen
			{
				System.out.println("20 CENT");
				rueckgabebetrag -= 0.2;
			}
			while (rueckgabebetrag >= 0.1) // 10 CENT-Müzen
			{
				System.out.println("10 CENT");
				rueckgabebetrag -= 0.1;
			}
			while (rueckgabebetrag >= 0.05)// 5 CENT-muenzen
			{
				System.out.println("5 CENT");
				rueckgabebetrag -= 0.05;
			}
		}
	}

	/**
	 * Main Methode, die alles aufruft
	 * 
	 * @param args, Startargumente, werden ignoriert
	 */
	public static void main(String[] args) {

		double zuZahlenderBetrag;
		double rueckgabebetrag;

		do {
			zuZahlenderBetrag = fahrkartenbestellungErfassen();
			rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			fahrkartenAusgeben();
			rueckgeldAusgeben(rueckgabebetrag);

			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir w�nschen Ihnen eine gute Fahrt.");
		} while (true);
		
	}
}